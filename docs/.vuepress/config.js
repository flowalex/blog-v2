module.exports = {
  title: "VuePress template",
  description:
    "A JAMstack website template with the default VuePress theme and Netlify CMS config.",
  //   base: "/VuePress-with-Netlify-CMS/",
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "About", link: "/about.html" },
      { text: "Projects", link: "/projects/" },
      { text: "Blog", link: "/blog/" },
      { text: "Guide", link: "/guide/" },
      { text: "Resume", link: "/resume.html" },
      {
        text: "Gitlab",
        link: "https://gitlab.com/flowalex/blog-v2",
      },
    ],

  },
  theme: 'vuepress-theme-solarized',
  globalUIComponents: ['ThemeManager'],
  plugins: {
    'vuepress-plugin-code-copy': true,
    'img-lazy':true,
    'seo': {
      siteTitle: (_, $site) => $site.title,
      title: $page => $page.title,
      description: $page => $page.frontmatter.description,
      author: (_, $site) => $site.themeConfig.author,
      tags: $page => $page.frontmatter.tags,
      twitterCard: _ => 'summary_large_image',
      type: $page => ['articles', 'posts', 'blog'].some(folder => $page.regularPath.startsWith('/' + folder)) ? 'article' :   'website',
      url: (_, $site, path) => ($site.themeConfig.domain || '') + path,
      image: ($page, $site) => $page.frontmatter.image && (($site.themeConfig.domain && !$page.frontmatter.image.startsWith  ('http') || '') + $page.frontmatter.image),
      publishedAt: $page => $page.frontmatter.date && new Date($page.frontmatter.date),
      modifiedAt: $page => $page.lastUpdated && new Date($page.lastUpdated) ,
    },
  },
};
