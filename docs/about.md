---
title = "About"
---

I am a System Engineer from Kansas City. In my free time, I enjoy cooking, biking, and the outdoors.

