---
title: Contact Me
date: 2020-12-20
---

<form accept-charset="UTF-8" action="https://getform.io/f/e9cd03f1-e6dc-4d9c-9847-c0b6dcc8c999" method="POST">
    <input type="email" name="email" placeholder="Your Email">
    <input type="text" name="name" placeholder="Your Name">
    <input type="text" name="message" placeholder="Your Message">
    <button type="submit">Send</button>
</form>
