---
title = "Resume"
---

### EDUCATION

DePaul University | **June 2018**  BS Information Technology

### EXPERIENCE

Cerner Corportaion | **System Enginner** *August 2018 - Present*

* Maintained over 2000 VMs though the use of automation
* Worked with colleagues across the globe
* Migrated VM platform to Kubernetes


Minneapolis Holidazzle | **Ice Rink Manager** *Seasonally 2014 – 2017*

* Maintained Quality of the ice rink
* Ensured safe use of the facility
* Praised for my dedication and reliability

Computex | **Computer Disassembler** *June 2014 – December 2014*

* Tear down and pack computers to be moved to a new location
* Set up computers once they have arrived at the new location

### SKILLS

* Experience with Ruby, Bash and Python Scripting
* Experience with Ubuntu and Red Hat Enterprise Linux Operating Systems
* Experience with Latest Windows Operating Systems
* Experience with building and deploying docker containers manually and through docker-compose
* Experience building and maintaining a Kubernetes clusters

